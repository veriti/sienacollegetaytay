<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'siena_db');

/** MySQL database username */
define('DB_USER', 'siena');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2hL<5[b&#g,]9TfaC F#9s:}DMG++kFJyY}+W=aQr,F}u5@h&oEJ,PBWj0cKq9T;');
define('SECURE_AUTH_KEY',  'Zw@(r]1@VVlE767&|t=}L63?{lvjDjz&C&A-f>yBB[PZtmRYba>yqK:k<DP*s^6b');
define('LOGGED_IN_KEY',    'h>EQEzJ7Wd9:v3I~@7Rc}(#3{lVto/L/4O?C/L~-fiS0q`:2Q!n`f@oxliyh.HGs');
define('NONCE_KEY',        'ryMPD(q#WA  I,EH6kP/QvsG(jz04r#r)#,yQGPZKh&@EbDJ&361I|^zi&cRS$%m');
define('AUTH_SALT',        'mJ;V-2,`?7pEw)piIB6#,5il2qe`}Qhm+&vu4u3f*{V!)/Rt&%6m<_j2ujYa aza');
define('SECURE_AUTH_SALT', 'u>n:5lD_N=+V0F1M]Bj<zqLg#s_J5i#J1]`z>4FH97h~$FHbJGh9X?_n8,<1M&79');
define('LOGGED_IN_SALT',   '*L/1jYf<5Tg6-1eA_] 65kYZq2Pgs~R`dO`v82&1zyj&%0l;?yW*tnkMMVnZQ#Uh');
define('NONCE_SALT',       'DVz<z389p7Fk,z_a>*Iy#9!LbvE@Z$`rv}y+Dx~P#|9zpKl{6Mhm`!+,Pn?DOe!V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
