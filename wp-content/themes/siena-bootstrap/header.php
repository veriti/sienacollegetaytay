<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Siena College</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->


  <?php wp_head(); ?>

  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/main.css">
  
</head>
<body>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <header>
      <div class="navbar" role="navigation">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="col-sm-2 col-md-1 navbar-brand">
                <a href="#"><img src="img/logo-siena-college.png" alt="Siena College"></a>
              </div>
              <div class="col-sm-6 col-md-7 col-lg-8 navbar-name">
                <a href="#"><img src="img/sienacollege-name.png" alt="Siena College"></a>
              </div>
              <div class="col-sm-4 col-md-4 col-lg-3 navbar-tel">
                <div class="col-lg-12 nav-tel-set">
                  <div class="contact-icon">
                    <span class="tel-icon icon-top-pos"></span>
                  </div>
                  <div class="contact-text">
                    <p><a href="tel:(02)658-8765">(02)658-8765</a></p>
                  </div>
                </div>
                <div class="col-lg-12 nav-tel-set">
                  <div class="contact-icon">
                    <span class="tel-icon icon-top-pos"></span>
                  </div>
                  <div class="contact-text">
                    <p><a href="tel:(02)660-4761">(02)660-4761</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <!-- /.nav-collapse -->
        </div>
        <div class="navigation-menu">
          <div class="container">
            <div class="row">
              <div class="collapse navbar-collapse nav-menu">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="index.html">Home</a></li>
                  <li><a href="#about-summary">About</a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admission<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Announcements</a></li>
                      <li><a href="#">Enrolement Procedures</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Academic Program<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Service Education Department</a></li>
                      <li><a href="#">Basic Education Department</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Students</a></li>
                  <li><a href="#">Research</a></li>
                  <li><a href="#">CCFDS</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container -->
      </div>
      <!-- /.navbar -->
    </header>