  <?php get_header(); ?>
  <section id="main-content">
  <div class="container">
    <div class="row row-offcanvas row-offcanvas-left">
      <!-- quick access nav -->
      <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <div class="list-group quick-access">
          <a href="#" class="list-group-item">Antipolo Help Ads <br>Siena College of Taytay</a>
          <a href="#" class="list-group-item">Check Mail</a>
          <a href="#" class="list-group-item">e-Learning</a>
          <a href="#" class="list-group-item">INSCRIBE</a>
          <a href="#" class="list-group-item">Online Public Access Catalog</a>
          <a href="#" class="list-group-item">St. Jaques Training Hotel <br>Facebook Page</a>
        </div>
      </div>
      <!--/ quick access nav -->
      <!-- featured contents -->
      <div class="col-xs-12 col-sm-9">
        <p class="pull-left visible-xs">
          <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Quick Links</button>
        </p>
        <!-- hero slider -->
        <div class="slider">
          <img src="<?php echo get_template_directory_uri(); ?>/img/slider-1.jpg">
        </div>
        <!--/ hero slider -->
        <div class="info-title">
          <span class="red-bg">Latest News</span>
        </div>
        <!-- featured banner -->
        <div class="col-xs-12 featured">
          <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/img-1.jpg"></a>
          <div class="caption">
            <hr class="yellow-line">
            <h2>Be A Part of Our Team!</h2>
            <p>Human Resource Management &amp; Development Siena College of Taytay is now accepting applicants for the following openings for SY 2017-2018...<a href="#"> Read more</a></p>
          </div>
        </div>
        <!--/ featured banner -->
        <div class="row">
          <div class="col-xs-12">
            <div class="row">
              <div class="col-md-6 info">
                <div class="zoom-bg">
                  <a href="#">
             <img src="<?php echo get_template_directory_uri(); ?>/img/img-2.jpg">
            <div class="caption">
              <h2>27th Family Rosary Crusade</h2>
            </div>
            </a>
                </div>
              </div>
              <!--/span-->
              <div class="col-md-6 info">
                <div class="zoom-bg">
                  <a href="#">
            <img src="<?php echo get_template_directory_uri(); ?>/img/img-3.jpg">
            <div class="caption">
              <h2>Siena College of Taytay Pushes Sixty</h2>
            </div>
            </a>
                </div>
              </div>
              <!--/span-->
              <div class="col-md-6 info">
                <div class="zoom-bg">
                  <a href="#">
            <img src="<?php echo get_template_directory_uri(); ?>/img/img-4.jpg">
            <div class="caption">
              <h2>Mikrotik Academy</h2>
            </div>
            </a>
                </div>
              </div>
              <!--/span-->
              <div class="col-md-6 info">
                <div class="zoom-bg">
                  <a href="#">
            <img src="<?php echo get_template_directory_uri(); ?>/img/img-5.jpg">
            <div class="caption">
              <h2>Mascot Design Competition</h2>
            </div>
            </a>
                </div>
              </div>
              <!--/span-->
              <div class="col-xs-12 read-more">
                <p><a class="btn btn-primary" href="#" role="button">See All &raquo;</a></p>
              </div>
              <!--/span-->
            </div>
          </div>
        </div>
      </div>
      <!--/ featured contents -->
    </div>
    <!--/ row-offcanvas -->
  </div>
  <!--/ container -->
</section>
<section id="about-summary">
  <div class="container">
    <h3 class="head-title">About</h3>
    <hr class="rule">
    <div class="row">
      <div class="col-md-3">
        <div class="profile">
          <img src="<?php echo get_template_directory_uri(); ?>/img/mother.jpg" alt="">
        </div>
      </div>
      <div class="col-md-9">
        <blockquote>
          <p>Dear Fellow Marian Devotees,</p>
          <p>
            Greetings of great joy as we celebrate FRC @ 25 here at Siena College of Taytay! This is a profound thanksgiving to our loving God for the Great Gift of His Mother and our Mother, too, from our school community for the past 25 years–and still going strong! We also thank God for the Gift of the Blessed Mother who leads us ever closer to her Son, Jesus.
          </p>
          <footer>&mdash; M. Teresa E. Examen, OP President
          </footer>
        </blockquote>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5 col-md-offset-1 sec-vision">
        <h4>Vision</h4>
        <p>We the members of Siena College Taytay, Taytay, Rizal, a catholic educational institution of the Dominican Sisters of St. Catherine of Siena – Philippines, are deeply committed in the formation of a Christ-centered integrated person and in the evangelizing mission of Church. Inspired by the Dominican Passion for Truth and Compassion for Humanity and the Missionary Spirit of Mother Francisca del Espiritu Santo de Fuentes, we envision a transformed society built on love, justice, peace and integrity of creation.</p>
      </div>
      <div class="col-md-5 sec-mission">
        <h4>Mission</h4>
        <p>Hence, we commit to become a Center of Excellence, espousing family values and life-oriented education through dynamic management, empowered personnel, relevant, and innovative curricula towards the formation of compassionate and globally competent Christians, who are environmentally and socially concerned, technology enabled, research motivated and Marian inspired.</p>
      </div>
    </div>
    <p>&nbsp;</p>
  </div>
</section>
<section id="about-access">
  <div class="col-md-4">
    <div class="row">
      <div class="zoom-bg">
        <a href="#">
            <img src="<?php echo get_template_directory_uri(); ?>/img/img-6.jpg">
            <div class="caption">
              <h4>History</h4>
            </div>
            </a>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="row">
      <div class="zoom-bg">
        <a href="#">
            <img src="<?php echo get_template_directory_uri(); ?>/img/img-7.jpg">
            <div class="caption">
              <h4>Campus</h4>
            </div>
            </a>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="row">
      <div class="zoom-bg">
        <a href="#">
            <img src="<?php echo get_template_directory_uri(); ?>/img/img-3.jpg">
            <div class="caption">
              <h4>Administration</h4>
            </div>
            </a>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>