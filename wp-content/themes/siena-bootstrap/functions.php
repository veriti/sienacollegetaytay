<?php
function b2w_theme_style() {

	wp_enqueue_style('bootstrap_css', get_template_directory_uri() .'/css/bootstrap.min.css' );
	wp_enqueue_style('bootstrap_css', get_template_directory_uri() .'/css/main.css' );
	wp_enqueue_style('style_css', get_template_directory_uri() .'/style.css' );

}

add_action ('wp_enqueue_scripts', 'b2w_theme_style');

function b2w_theme_js() {

	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() .'https://code.jquery.com/jquery-1.10.2.min.js', array('jquery'),'',true );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() .'js/bootstrap.min.js', array('jquery'),'',true );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() .'js/main.js', array('jquery'),'',true );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() .'https://use.fontawesome.com/98b7e578c8.js', array('jquery'),'',true );
}


add_action('wp_enqueue_scripts','b2w_theme_js');

?>